rm -rf /opt/ANDRAX/dnstwist

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/pip3 install -r requirements.txt

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Pip instal... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

cp -Rf $(pwd) /opt/ANDRAX/dnstwist

cp -Rf docs/dnstwist.1 /usr/local/share/man/man1/

mandb

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
